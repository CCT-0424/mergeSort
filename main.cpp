#include <iostream>

using namespace std;

const int tamanho=10;

int vetor[tamanho];

void lerVetor(int vetor[],int tamanho){
    for (int i=0; i < tamanho; i++){
        cout << "Vetor["<<(i+1)<<"]: ";
        cin >> vetor[i];
    }
}

void imprimirVetor(int vetor[],int tamanho){
    for (int i=0; i < tamanho; i++){
        cout << "Vetor["<<(i+1)<<"]: "<<vetor[i]<<endl;
    }
}

void merge(int vetor[],int inicio, int fim, int meio){

    int i; //indice do vetor externo (menor);
    int j; //indice do vetor externo (maior);
    int k; //indice do vetor interno;
    int temp[(fim-inicio)+1]; //vetor temporário

    i = inicio;
    j = meio+1;
    k = 0;

	while (i <= meio && j <= fim)
	{
		if (vetor[i] < vetor[j])
		{
			temp[k] = vetor[i];
			k++;
			i++;
		}
		else
		{
			temp[k] = vetor[j];
			k++;
			j++;
		}
	}

	while (i <= meio)
	{
		temp[k] = vetor[i];
		k++;
		i++;
	}

	while (j <= fim)
	{
		temp[k] = vetor[j];
		k++;
		j++;
	}

	for (i = inicio; i <= fim; i++)
	{
		vetor[i] = temp[i-inicio];
	}

}

void mergeSort(int vetor[],int inicio, int fim){

    if (inicio < fim){
        int meio = (inicio+fim)/2;

        mergeSort(vetor,inicio,meio);
        mergeSort(vetor,meio+1,fim);

        merge(vetor,inicio,fim,meio);

    }

}

int main()
{
    lerVetor(vetor,tamanho);
    mergeSort(vetor,0,tamanho-1);
    imprimirVetor(vetor,tamanho);
    cout << "Fim" << endl;
    return 0;
}
